return {
  'folke/tokyonight.nvim',
  lazy = false,
  priority = 1000,
  opts = {},
  config = function()
    local tokyonight = require 'tokyonight'

    tokyonight.setup {
      style = 'moon',
    }

    vim.cmd.colorscheme 'tokyonight-moon'
    vim.api.nvim_set_hl(0, 'FloatBorder', { link = 'Normal' }) -- line to fix the background color border
  end,
}
