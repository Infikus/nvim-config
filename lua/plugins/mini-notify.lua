return {
  'echasnovski/mini.notify',
  version = '*',
  config = function()
    require('mini.notify').setup()
    vim.notify = require('mini.notify').make_notify {
      ERROR = { duration = 5000, hl_group = 'DiagnosticError' },
      WARN = { duration = 3000, hl_group = 'DiagnosticWarn' },
      INFO = { duration = 2000, hl_group = 'DiagnosticInfo' },
      DEBUG = { duration = 0, hl_group = 'DiagnosticHint' },
      TRACE = { duration = 0, hl_group = 'DiagnosticOk' },
      OFF = { duration = 0, hl_group = 'MiniNotifyNormal' },
    }
  end,
}
