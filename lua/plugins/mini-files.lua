return {
  'echasnovski/mini.files',
  version = '*',
  keys = {
    {
      '<leader>e',
      function()
        require('mini.files').open()
      end,
    },
  },
  config = function()
    require('mini.files').setup {
      mappings = {
        go_in_plus = '<Right>',
        go_out = '<Left>',
        close = '<Esc>',
      },
    }
  end,
}
