return {
  'nvim-treesitter/nvim-treesitter',
  event = { 'BufReadPre', 'BufNewFile' },
  build = ':TSUpdate',
  dependencies = {
    'windwp/nvim-ts-autotag',
    'JoosepAlviste/nvim-ts-context-commentstring',
  },
  config = function()
    local ts = require 'nvim-treesitter.configs'

    ts.setup {
      auto_install = true,
      highlight = {
        enable = true,
      },
      indent = { enable = true },
      autotag = {
        enable = true,
      },
      ensure_installed = {
        'json',
        'javascript',
        'typescript',
        'tsx',
        'yaml',
        'html',
        'css',
        'markdown',
        'svelte',
        'graphql',
        'lua',
        'dockerfile',
        'gitignore',
        'rust',
        'go',
        'sql',
        'toml',
        'php',
        'vue',
      },
    }

    require('ts_context_commentstring').setup {}
  end,
}
