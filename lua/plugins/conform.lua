return {
  'stevearc/conform.nvim',
  lazy = false,

  keys = {
    { '<leader>F', '<CMD>lua require("conform").format({ lsp_fallback = true })<CR>', desc = 'Format code' },
  },
  opts = {
    formatters_by_ft = {
      lua = { 'stylua' },
      javascript = { 'prettier' },
      typescript = { 'prettier' },
      javascriptreact = { 'prettier' },
      typescriptreact = { 'prettier' },
      svelte = { 'prettier' },
      csso = { 'prettier' },
      html = { 'prettier' },
      json = { 'prettier' },
      yaml = { 'prettier' },
      markdown = { 'prettier' },
      graphql = { 'prettier' },
      python = { 'isort', 'black' },
      php = { 'easy-coding-standard' },
      go = { 'gofmt' },
    },
    format_on_save = {
      timeout_ms = 2000,
      lsp_fallback = true,
    },
  },
}
