return {
  'echasnovski/mini.completion',
  version = '*',
  config = function()
    require('mini.completion').setup {
      delay = { completion = 10 ^ 7 },
      window = {
        signature = { border = 'rounded' },
      },
    }
  end,
}
