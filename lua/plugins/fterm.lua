return {
  'numToStr/FTerm.nvim',
  lazy = false,
  enabled = true,
  keys = {
    { '<leader>tf', '<CMD>lua require("FTerm").toggle()<CR>', desc = 'Open terminal' },
  },
  opts = {
    border = 'rounded',
  },
}
