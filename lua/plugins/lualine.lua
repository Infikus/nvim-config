return {
  'nvim-lualine/lualine.nvim',
  config = function()
    local lualine = require 'lualine'

    lualine.setup {
      option = {
        icons_enabled = true,
        theme = 'tokyonight',
      },
    }
  end,
}
