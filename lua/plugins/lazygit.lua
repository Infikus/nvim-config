return {
  'kdheepak/lazygit.nvim',
  depenencies = {
    'nvim-lua/plenary.nvim',
  },
  keys = {
    { '<leader>gg', '<CMD>LazyGit<CR>', desc = 'Open LazyGit' },
  },
}
